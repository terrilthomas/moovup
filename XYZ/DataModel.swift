//
//  DataModel.swift
//  XYZ
//
//  Created by Terril Palliparambil on 9/4/2018.
//  Copyright © 2018 Terril Palliparambil. All rights reserved.
//

import Foundation
import ObjectMapper

class DataModel: Mappable {
    var location: LocationModel?
    var picture: String?
    var name: String?
    var _id: String?
    var email: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        location <- map["location"]
        picture <- map["picture"]
        name <- map["name"]
        _id <- map["_id"]
        email <- map["email"]
    }
}

class LocationModel : Mappable {
    var latitude : Double?
    var longitude : Double?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
