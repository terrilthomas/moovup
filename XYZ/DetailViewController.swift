//
//  DetailViewController.swift
//  XYZ
//
//  Created by Terril Palliparambil on 9/4/2018.
//  Copyright © 2018 Terril Palliparambil. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import Kingfisher

class DetailViewController : UIViewController {
    
    var dataModel :  DataModel! = nil
    let imgUser = UIImageView()
    let labelName = UILabel()
    var mapView = GMSMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.title = "Your Friend"
        self.view.backgroundColor = UIColor.white
        print("Latitude \(dataModel.location?.latitude) and Longitude \(dataModel.location?.longitude)")
        let camera = GMSCameraPosition.camera(withLatitude: (dataModel.location?.latitude)!,
                                              longitude:  (dataModel.location?.longitude)!,
                                              zoom: 14)
        mapView = GMSMapView.map(withFrame: .zero, camera: camera)
        
        let marker = GMSMarker()
        marker.position = camera.target
        marker.snippet = dataModel.name
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.map = mapView
        
        let screenSize: CGRect = UIScreen.main.bounds
        
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        mapView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight / 2)
        self.view.addSubview(mapView)
        
        belowMapContent()
        
    }
    
    private func belowMapContent() {
        imgUser.translatesAutoresizingMaskIntoConstraints = false
        labelName.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(imgUser)
        view.addSubview(labelName)
        
        let viewsDict = [
            "image" : imgUser,
            "username" : labelName,
            "mapview" : mapView,
            ] as [String : Any]
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[mapview]-(<=10)-[image(80)]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[mapview]-(<=30)-[username]", options: [], metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[image(80)]-[username]|", options: [], metrics: nil, views: viewsDict))
        
        let url = URL(string: self.dataModel.picture!)
        let processor = RoundCornerImageProcessor(cornerRadius: 20)
        imgUser.kf.setImage(with: url, placeholder: nil, options: [.processor(processor)])
        
        labelName.text = self.dataModel.name
        labelName.textColor = UIColor.black
    }
}
