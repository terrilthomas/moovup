//
//  DataViewCell.swift
//  XYZ
//
//  Created by Terril Palliparambil on 9/4/2018.
//  Copyright © 2018 Terril Palliparambil. All rights reserved.
//

import Foundation
import UIKit

class DataViewCell: UITableViewCell {
    
    let imgUser = UIImageView()
    let labelName = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imgUser.translatesAutoresizingMaskIntoConstraints = false
        labelName.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(imgUser)
        contentView.addSubview(labelName)
        
        let viewsDict = [
            "image" : imgUser,
            "username" : labelName,
            ] as [String : Any]
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[image(40)]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(<=20)-[username]", options: [], metrics: nil, views: viewsDict))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[image(40)]-[username]|", options: [], metrics: nil, views: viewsDict))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
