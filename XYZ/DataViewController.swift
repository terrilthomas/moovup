//
//  ViewController.swift
//  XYZ
//
//  Created by Terril Palliparambil on 9/4/2018.
//  Copyright © 2018 Terril Palliparambil. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import Kingfisher

class DataViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   
    var dataTableView: UITableView  =   UITableView()
    var dataArray : [DataModel] = [DataModel]()
    let TABLE_CELL_IDENTIFIER : String = "dataCell"
    let DATA_API_URL = "http://www.json-generator.com/api/json/get/cfdlYqzrfS"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        Alamofire.request(DATA_API_URL).responseArray { (response: DataResponse<[DataModel]>) in
            
            self.dataArray = response.result.value!
            debugPrint(response)
            self.dataTableView.reloadData()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.title = "All Friends"
        // Get main screen bounds
        let screenSize: CGRect = UIScreen.main.bounds
        
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        dataTableView.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight)
        dataTableView.dataSource = self
        dataTableView.delegate = self
    
        dataTableView.register(DataViewCell.self, forCellReuseIdentifier: TABLE_CELL_IDENTIFIER)
        
        self.view.addSubview(dataTableView)
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return dataArray.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let destination = DetailViewController() // Your destination
        destination.dataModel = dataArray[indexPath.row]
        navigationController?.pushViewController(destination, animated: true)
        print("User selected table row \(indexPath.row) and item \(dataArray[indexPath.row])")
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TABLE_CELL_IDENTIFIER, for: indexPath as IndexPath) as! DataViewCell
        let url = URL(string: self.dataArray[indexPath.row].picture!)
        
        let processor = RoundCornerImageProcessor(cornerRadius: 20)
        cell.imgUser.kf.setImage(with: url, placeholder: nil, options: [.processor(processor)])
        
        cell.labelName.text = self.dataArray[indexPath.row].name
        
        return cell
    }
}

